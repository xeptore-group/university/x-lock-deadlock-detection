import random
import threading
import time
import psycopg2

salary_inc_worker_state = (None, None)
age_inc_worker_state = (None, None)


AGE_INCREMENT_THREAD_ID = 1
SALARY_INCREMENT_THREAD_ID = 1


def create_age_inc_worker(db_cursor, commit):
    def run():
        global age_inc_worker_state
        age_inc_worker_state = ("running", None)
        try:
            while True:
                random_row_id = random.randint(1, 100)
                while True:
                    db_cursor.execute(
                        f"SELECT x_locker_thread_id FROM employees WHERE id = {random_row_id}"
                    )
                    (locker_thread_id,) = db_cursor.fetchone()
                    if locker_thread_id is None:
                        db_cursor.execute(
                            f"UPDATE employees SET x_locker_thread_id = {AGE_INCREMENT_THREAD_ID} WHERE id = {random_row_id}"
                        )
                        commit()
                        age_inc_worker_state = ("running", None)
                        break
                    else:
                        age_inc_worker_state = ("waiting", locker_thread_id)
                db_cursor.execute(
                    f"UPDATE employees SET age = age + 1 WHERE id = {random_row_id}"
                )
                commit()
        except:
            db_cursor.close()

    return run


def create_salary_inc_worker(db_cursor, commit):
    def run():
        global salary_inc_worker_state
        salary_inc_worker_state = ("running", None)
        try:
            while True:
                random_row_id = random.randint(1, 100)
                while True:
                    db_cursor.execute(
                        f"SELECT x_locker_thread_id FROM employees WHERE id = {random_row_id}"
                    )
                    (locker_thread_id,) = db_cursor.fetchone()
                    if locker_thread_id is None:
                        db_cursor.execute(
                            f"UPDATE employees SET x_locker_thread_id = {SALARY_INCREMENT_THREAD_ID} WHERE id = {random_row_id}"
                        )
                        commit()
                        salary_inc_worker_state = ("running", None)
                        break
                    else:
                        salary_inc_worker_state = ("waiting", locker_thread_id)
                db_cursor.execute(
                    f"UPDATE employees SET salary = salary + 1 WHERE id = {random_row_id}"
                )
                commit()
        except:
            db_cursor.close()

    return run


def create_deadlock_detector():
    def run():
        while True:
            time.sleep(1)
            (age_state, age_forwhat) = age_inc_worker_state
            (salary_state, salary_forwhat) = salary_inc_worker_state
            if age_state == "waiting" and salary_state == "waiting":
                if (
                    age_forwhat == SALARY_INCREMENT_THREAD_ID
                    and salary_forwhat == AGE_INCREMENT_THREAD_ID
                ):
                    print("DEADLOCK: this wait will never end!")
                    exit(1)

    return run


def prepare_db(connection):
    cursor = connection.cursor()
    cursor.execute("DROP TABLE IF EXISTS employees CASCADE;")
    cursor.execute(
        """
    CREATE TABLE employees (
        id SERIAL PRIMARY KEY NOT NULL,
        name                    varchar(128),
        age                     INT     NOT NULL,
        salary                  REAL,
        x_locker_thread_id         INT
    );
        """
    )
    with open("./data.csv", "r+") as data:
        lines = [line.split(",") for line in data.readlines()]
        cursor.execute(
            f"""
            INSERT INTO employees (name, age, salary, x_locker_thread_id) VALUES
            {','.join(
                list(
                    map(
                        lambda record: "('{name}', {age}, {salary}, NULL)".format(name=record[0].replace("'", ""), age=record[1], salary=record[2]),
                        lines
                    )
                )
            )}
            """
        )

    connection.commit()
    cursor.close()


def main():
    connection = psycopg2.connect(
        database="postgres",
        user="admin",
        password="secret",
        host="localhost",
        port="5432",
    )

    prepare_db(connection)

    age_inc_worker_thread = threading.Thread(
        target=create_age_inc_worker(connection.cursor(), connection.commit), args=()
    )
    salary_inc_worker_thread = threading.Thread(
        target=create_salary_inc_worker(connection.cursor(), connection.commit), args=()
    )
    deadlock_detector_worker_thread = threading.Thread(
        target=create_deadlock_detector(), args=()
    )

    age_inc_worker_thread.start()
    salary_inc_worker_thread.start()
    deadlock_detector_worker_thread.start()

    deadlock_detector_worker_thread.join()

    connection.close()


if __name__ == "__main__":
    main()
