# X-Lock Deadlock Detection 🔒 💀 🔍

Database X-Lock Deadlock Detection Simulation Educational Purpose Project

## Run 🤔

### Prerequisites 🛠️

- [Docker Compose](https://docs.docker.com/compose/install/)
- [Python](https://www.python.org/downloads/)
- [Pipenv](https://pipenv.pypa.io/en/latest)

### Running 🏃

Execute following commands in the project's root directory:

```sh
docker-compose up --detach
pipenv install
pipenv shell
python main.py
```

You will eventually see the following message gets printed when a deadlock occurs:

```txt
DEADLOCK: this wait will never end!
```
